import 'react-native-gesture-handler';
import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
//Views
import NuevaOrden from './views/NuevaOrden';
import Menu from './views/Menu';
import DetallePlatillo from './views/DetallePlatillo';
import FormularioPlatillo from './views/FormularioPlatillo';
import ResumenPedido from './views/ResumenPedido';
import ProgresoPedido from './views/ProgresoPedido';
import BotonResumen from './components/BotonResumen';

//state de context
import FirebaseState from './context/firebase/firebaseState';
import PedidoState from './context/pedidos/pedidosState';
import { NativeBaseProvider } from 'native-base';


const Stack = createStackNavigator();

const App = () => {
  return (
    <NativeBaseProvider>
      <FirebaseState>
        <PedidoState>
          <NavigationContainer>
            <Stack.Navigator
              screenOptions={{
                headerStyle: {
                  backgroundColor: '#FFDA00'
                },
                headerTitleStyle: {
                  fontWeight: 'bold'
                },
                headerTintColor: '#000'
              }}
            >
              <Stack.Screen name="NuevaOrden" component={NuevaOrden} options={{headerShown: false}}/>
              <Stack.Screen 
                name="Menu" 
                component={Menu} 
                options={{
                  title: 'Menu',
                  headerRight: props => <BotonResumen />
                }}
              />
              <Stack.Screen name="DetallePlatillo" component={DetallePlatillo} options={{title: 'Detalle del pedido'}}/>
              <Stack.Screen name="FormularioPlatillo" component={FormularioPlatillo} options={{title: 'Formulario'}}/>
              <Stack.Screen name="ResumenPedido" component={ResumenPedido} options={{title: 'Resumen'}}/>
              <Stack.Screen name="ProgresoPedido" component={ProgresoPedido} options={{title: 'Progreso del pedido'}}/>
            </Stack.Navigator>
          </NavigationContainer>
        </PedidoState>
      </FirebaseState>
    </NativeBaseProvider>
  );
};

const styles = StyleSheet.create({

});

export default App;
