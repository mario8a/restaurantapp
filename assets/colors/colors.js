const colors = {
  background: '#F9F9FB',
  while: '#FFF',
  text: '#313234',
  primary: '#F5CA48',
  secondary: '#F26C68',
  textLight: '#CDCDCD',
  price: '#E4723C'
}

export default colors;