import React, {useContext} from 'react';
import {Button, Text} from 'native-base';
import globalStyles from '../styles/globalStyles';
import { useNavigation } from '@react-navigation/native';
import PedidoContext from '../context/pedidos/pedidosContext';


const BotonResumen = () => {

  const navigation = useNavigation();

  const { pedido } = useContext(PedidoContext);

  if (pedido.length === 0) return null

  return (
    <Button style={globalStyles.boton} onPress={() => navigation.navigate('ResumenPedido')} >
      <Text style={{color: 'black', fontWeight: 'bold', fontSize: 18}}>Pedido</Text>
    </Button>
  )
}

export default BotonResumen