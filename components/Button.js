import React from 'react';
import { View, StyleSheet, Pressable, Text, SafeAreaView, Image, TouchableOpacity } from 'react-native';
import colors from '../assets/colors/colors';


const PrimaryButton = ({title, onPress = () => {}}) => {
  return (
    <TouchableOpacity activeOpacity={0.8} onPress={onPress} >
      <View style={styles.btnContainer}>
        <Text style={styles.title} >{title}</Text>
      </View>
    </TouchableOpacity>
  )
};


const styles = StyleSheet.create({
  btnContainer: {
    backgroundColor: colors.primary,
    height: 60,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    color: colors.while,
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold'
  }
})

export default PrimaryButton