import React, {useReducer} from 'react';
import _ from 'lodash';

import firebase from '../../firebase';
import FirebaseContext from './firebaseContext';
import FirebaseReducer from './firebaseReducer';

import { OBTENER_PRODUCTOS_EXITO } from '../../types';

const FirebaseState = ({children}) => {

  // State inicial
  const initialState = {
    menu: []
  }

  //useReducer con dispatch para ejecutar las funciones
  const [ state, dispatch ] = useReducer(FirebaseReducer, initialState);

  // Funcions que se ejecuta para traer los productos
  const obtenerProductos = () => {
    firebase.db
      .collection('productos')
      .where('existencia', '==', true) // traer solo los que esten en existencia
      .onSnapshot(manejarSnapshot);

    function manejarSnapshot(snapshot) {
      let platillos = snapshot.docs.map(doc => {
        return{
          id: doc.id,
          ...doc.data()
        }
      });

      platillos = Array.from(
        platillos.reduce(
          (m, {categoria, ...data}) =>
            m.set(categoria, [...(m.get(categoria) || []), data]),
          new Map(),
        ),
        ([categoria, data]) => ({categoria, data}),
      );

      dispatch({
        type: OBTENER_PRODUCTOS_EXITO,
        payload: platillos
      });
    }
  }


  return (
    <FirebaseContext.Provider
      value={{
        menu: state.menu,
        firebase,
        obtenerProductos
      }}
    >
      {children}
    </FirebaseContext.Provider>
  )
}

export default FirebaseState;