import { StyleSheet } from 'react-native';

const globalStyles = StyleSheet.create({
  container: {
    flex: 1
  },
  contenido: {
    marginHorizontal: '2.5%',
    flex: 1,
  },
  boton: {
    backgroundColor: '#FFDA00',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  botonTexto: {
    textTransform: 'uppercase',
    fontFamily: 'Montserrat-Bold',
    color: '#000'
  },
  cantidad: {
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold'
  },
  titulo: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 20,
  }
});

export default globalStyles;