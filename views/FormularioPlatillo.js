import React, {useState, useContext, useEffect} from 'react';
import {
  Input,
  Text,
  Flex,
  Center,
  Spacer,
  View,
  Pressable,
  Button
} from 'native-base';
import { Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import PedidoContext from '../context/pedidos/pedidosContext';
import globalStyles from '../styles/globalStyles';

const FormularioPlatillo = () => {

  const { platillo, guardarPedido } = useContext(PedidoContext);
  const { precio } = platillo;
  const [cantidad, setCantidad] = useState(1);
  const [total, setTotal] = useState(0);

  const navigation = useNavigation();

  useEffect(() => {
    calcualarTotal();
  }, [cantidad])
  

  const calcualarTotal = () => {
    const totalPagar = precio * cantidad;
    setTotal(totalPagar);
  }

  const decrementar = () => {
    if (cantidad > 1) {
      const nuevaCantidad = parseInt(cantidad) - 1;
      setCantidad(nuevaCantidad);
    }
  }
  const incrementar = () => {
    const nuevaCantidad = parseInt(cantidad) + 1;
    setCantidad(nuevaCantidad);
  }

  const confirmarOrden = () => {
    Alert.alert(
      '¿Deseas confirmar tu pedido?',
      'Un pedido confirmado ya no se podra modificar',
      [
        {
          text: 'Confirmar',
          onPress: () => {
            // Almancear el pedido principal
            const pedido = {
              ...platillo,
              cantidad,
              total
            }
            guardarPedido(pedido)
            // navegar hacia el resumen
            navigation.navigate('ResumenPedido');

          },
        },
        {
          text: 'Cancelar',
          style: 'cancel'
        }
      ]
    )
  }

  return (
    <View>
      <View>
        <Text style={{textAlign: 'center', fontSize: 20, marginTop: 20}}>Cantidad</Text>
        <Flex direction="row" mb="2.5" mt="1.5" justifyContent="space-evenly">
        <Pressable onPress={() => decrementar()}>
          <Center size={16} bg="primary.500" _dark={{
          bg: "primary.400"
          }} rounded="sm" _text={{
            color: "warmGray.50",
            fontWeight: "medium",
            fontSize: 40
          }}>
            -
          </Center>
        </Pressable>
        <Input
          fontSize={20} 
          mx="3"
          editable={false}
          w="20%"
          style={{textAlign: 'center'}}
          value={cantidad.toString()}
          keyboardType="number-pad"
          onChangeText={(cantidad) => setCantidad(cantidad)}
        />
        <Pressable onPress={() => incrementar()}>
          <Center size={16} bg="primary.500" _dark={{
          bg: "primary.400"
          }} rounded="sm" _text={{
            color: "warmGray.50",
            fontWeight: "medium",
            fontSize: 40
          }}>
            +
          </Center>
        </Pressable>
      </Flex>

      <Text style={globalStyles.cantidad}>Subtotal: ${total}</Text>
      <Spacer />
      <Button onPress={() => confirmarOrden()} >Agregar al pedido</Button>
      </View>
    </View>
  )
}

export default FormularioPlatillo