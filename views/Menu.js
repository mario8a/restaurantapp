import React, {useContext, useEffect} from 'react';
import { useNavigation } from '@react-navigation/native';
import globalStyles from '../styles/globalStyles';
import {
  Text,
  Box,
  Heading,
  FlatList,
  HStack,
  Avatar,
  Spacer,
  VStack,
  Center,
  SectionList,
  Flex,
  Divider,
  Pressable
} from 'native-base';

import FirebaseContext from '../context/firebase/firebaseContext';
import PedidoContext from '../context/pedidos/pedidosContext';

const Menu = () => {
  // Context de firebase
  const {obtenerProductos, menu} = useContext(FirebaseContext);
  // context pedido
  const { seleccionarPlatillo } = useContext(PedidoContext);

  // navegacion
  const navigation = useNavigation()

  useEffect(() => {
    obtenerProductos();
  }, []);


  return (
      <Center flex={1}>
        <SectionList
          w="100%"
          mx={3}
          sections={menu}
          keyExtractor={(item, index) => item + index}
          renderItem={({item}) => (
            <Pressable
            onPress={() => {
              seleccionarPlatillo(item);
              navigation.navigate('DetallePlatillo');
            }}
            >
              <Flex direction="row" >
                <Avatar
                  mx={10}
                  mt={2}
                  source={{
                    uri: item.imagen,
                  }}>
                  {item.nombre}
                </Avatar>
                <Flex direction="column" mt={1}>
                  <Text>{item.nombre}</Text>
                  <Text fontSize="xs" numberOfLines={2}>
                    {item.descripcion}
                  </Text>
                  <Text fontWeight="bold">Precio: ${item.precio}</Text>
                </Flex>
              </Flex>
              <Divider my={2} />
            </Pressable>
          )}
          renderSectionHeader={({section: {categoria}}) => (
            <Box
              bg="black"
              p={2}
              _text={{
                fontSize: 'sm',
                fontWeight: 'bold',
                color: '#FFDA00',
                textAlign: 'left',
              }}>
              {categoria}
            </Box>
          )}
        />
      </Center>
  );
};

export default Menu;
