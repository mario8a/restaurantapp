import React from 'react';
import { View, StyleSheet, Pressable, Text, SafeAreaView, Image } from 'react-native';
import globalStyles from '../styles/globalStyles';
import { useNavigation } from '@react-navigation/native';
import colors from '../assets/colors/colors';
import PrimaryButton from '../components/Button';
// import Icon from 'react-native-vector-icons/Ionicons';
// Icon.loadFont();


const NuevaOrden = () => {

  const navigation = useNavigation();

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: colors.background}}>
      <View style={{height: 400}}>
        <Image
          style={{
            width: '100%',
            resizeMode: 'contain',
          }}
          source={require('../assets/foodApp.png')}
        />
      </View>
      <View style={styles.textContainer}>
        <View>
          <Text style={{fontSize: 32, fontFamily: 'Montserrat-SemiBold', textAlign: 'center'}}>Delicious Food</Text>
          <Text style={{marginTop: 20, fontSize: 18, textAlign: 'center', color: colors.textLight}}>
            We help you to find best and delicious food
          </Text>
        </View>
        <PrimaryButton title="Get Started"  onPress={() => navigation.navigate('Menu')} />
      </View>
    </SafeAreaView>
  )
};

const styles = StyleSheet.create({
  textContainer: {
   flex: 1,
   paddingHorizontal: 50,
   justifyContent: 'space-between',
   paddingBottom: 40,
  }
})

export default NuevaOrden