import React, {useContext, useEffect, useState} from 'react';
import { Text, View, StyleSheet, Pressable} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import globalStyles from '../styles/globalStyles';
import PedidoContext from '../context/pedidos/pedidosContext';
import firebase from '../firebase';
import Countdown from 'react-countdown';

const ProgresoPedido = () => {

  const navigation = useNavigation();

  const { idPedido } = useContext(PedidoContext);
  const [tiempo, setTiempo] = useState(0);
  const [completado, setCompletado] = useState(false);

  useEffect(() => {
    const obtenerProducto = () => {
      firebase.db.collection('ordenes')
              .doc(idPedido)
              .onSnapshot(function(doc) {
                setTiempo(doc.data().tiempoEntrega);
                setCompletado(doc.data().completado)
              })
    }
    obtenerProducto();
  }, [])
  

  const renderer = ({minutes, seconds}) => {
    
    return (
      <Text style={styles.tiempo}>{minutes}:{seconds}</Text>
    )
  }

  return (
    <View style={globalStyles.container}>
      <View style={[globalStyles.contenido, {marginTop: 50}]}>
        {tiempo === 0 && (
          <>
            <Text style={{textAlign: 'center'}} >Hemos recibito tu orden...</Text>
            <Text style={{textAlign: 'center'}} >Estamos calculando tu tiempo de entrega</Text>
          </>
        ) }
        { !completado && tiempo > 0 && (
          <>
            <Text style={{textAlign: 'center'}} >Su orden estara lista en: </Text>
            <Text>
              <Countdown
                date={Date.now() + tiempo * 60000}
                renderer={renderer}
              />
            </Text>
          </>
        ) }

        { completado && (
          <>
            <Text style={styles.textoCompletado}>Orden lista</Text>
            <Text style={styles.textoCompletado}>Por favor, pase a recoger su pedido</Text>
            <View style={globalStyles.container}>
              <View style={[globalStyles.contenido, styles.contenido]}>
                <Pressable style={globalStyles.boton} onPress={() => navigation.navigate('NuevaOrden')}>
                  <Text style={globalStyles.botonTexto} >Nueva Orden</Text>
                </Pressable>
              </View>
            </View>
          </>
        ) }
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  tiempo: {
    marginBottom: 20,
    fontSize: 60,
    textAlign: 'center',
    marginTop: 30
  },
  textoCompletado: {
    textAlign: 'center',
    textTransform: 'uppercase',
    marginBottom: 20
  }
})

export default ProgresoPedido