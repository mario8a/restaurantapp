import React, { useContext, useEffect} from 'react';
import {
  Input,
  Text,
  Flex,
  Center,
  Spacer,
  View,
  Pressable,
  Button,
  SectionList,
} from 'native-base';
import { Alert, StyleSheet, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import firebase from '../firebase';

import PedidoContext from '../context/pedidos/pedidosContext';
import globalStyles from '../styles/globalStyles';


const ResumenPedido = () => {

  const navigation =  useNavigation();

  const { pedido, total, mostrarResumen, eliminarProducto, pedidoRealizado } = useContext(PedidoContext);

  useEffect(() => {
    calcularTotal();
  }, [pedido]);

  const calcularTotal = () => {
    let nuevoTotal = 0;
    nuevoTotal = pedido.reduce((nuevoTotal, articulo) => nuevoTotal + articulo.total, 0);
    mostrarResumen(nuevoTotal);
  }

  const progresoPedido = () => {
    Alert.alert(
      'Revisa tu pedido',
      'Una vez que realizas tu pedido, no podras cambiarlo',
      [
        {
          text: 'Confirmar',
          onPress: async () => {
            // crear obj
            const pedidoObj = {
              tiempoEntrega: 0,
              completado: false,
              total: Number(total),
              orden: pedido,
              creado: Date.now()
            }

            try {
              const pedido = await firebase.db.collection('ordenes').add(pedidoObj);
              pedidoRealizado(pedido.id);
              navigation.navigate('ProgresoPedido');
            } catch (error) {
              console.log(error)
            }            
          },
        },
        {
          text: 'Cancelar',
          style: 'cancel'
        }
      ]
    )
  };

  const confirmarEliminacion = (id) => {
    Alert.alert(
      'Deseas eliminar este articulo??',
      'Una vez que eliminado tu pedido, no podras recupearlo',
      [
        {
          text: 'Confirmar',
          onPress: () => {
            eliminarProducto(id);
          },
        },
        {
          text: 'Cancelar',
          style: 'cancel'
        }
      ]
    )
  }
  

  return (
    <View style={globalStyles.container}>
      <View style={globalStyles.contenido}>
        <Text style={globalStyles.titulo}>Resumen pedido</Text>

        <FlatList
          data={pedido}
          keyExtractor={(pedido) => pedido.id}
          renderItem={({item}) => (
              <View>
                <View>
                  <Text>{item.nombre}</Text>
                  <Text>{item.cantidad}</Text>
                  <Text>{item.precio}</Text>

                  <Button
                    onPress={() => confirmarEliminacion(item.id)}
                    colorScheme="secondary"
                  >
                    <Text style={{color: "#FFF"}}>Eliminar</Text>
                  </Button>
                </View>
              </View>
          )}
        />
        <Text>Total a pagar: ${total}</Text>

        <Pressable style={globalStyles.boton} onPress={() => navigation.navigate('Menu')}>
          <Text style={globalStyles.botonTexto} >Seguir pidiendo</Text>
        </Pressable>
        <View style={{marginVertical: 10}}>
          <Pressable style={globalStyles.boton} onPress={() => progresoPedido()}>
            <Text style={globalStyles.botonTexto} >Ordenar pedido</Text>
          </Pressable>
        </View>

      </View>
    </View>
  )
}

export default ResumenPedido